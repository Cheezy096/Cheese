import traceback
import sys
import discord
import json
import math
from discord.ext import commands
from Internal import checks
from config.confug import Set

config = Set()
name = config.name
ver = config.version

class CommandErrorHandler:
    def __init__(self, bot):
        self.bot = bot

    async def on_command_error(self, ctx, error):
        """The event triggered when an error is raised while invoking a command.
        ctx   : Context
        error : Exception"""

        if hasattr(ctx.command, 'on_error'):
            return

        ignored = ()
        error = getattr(error, 'original', error)

        if isinstance(error, ignored):
            print("This is disabled silly")
            return

        elif isinstance(error, commands.DisabledCommand):
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but that command is disabled.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)





        elif isinstance(error, commands.CommandNotFound):
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but that command doesn't exist.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)



        elif isinstance(error, commands.NoPrivateMessage):
            try:
                embed=discord.Embed(title="❌ That's an issue!", description=f"{ctx.command} can not be used in DM's.", colour=0xc00505)
                embed.set_footer(text=ver)
                return await ctx.author.send(embed=embed)
            except:
                pass

        elif isinstance(error, commands.BadArgument):
            embed = discord.Embed(title="❌ That's an issue!", description="Whoops! that was a BadArgument! please redo the command properly", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)

        elif isinstance(error, commands.NotOwner):
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but you can't run this command", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)


        elif isinstance(error, commands.MissingRequiredArgument):
            embed = discord.Embed(title="❌ That's an issue!", description="It seems you have forgotten something! MissingRequiredArgument.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)


        elif isinstance(error, commands.BotMissingPermissions):
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but i don't have the correct permissions to do that.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)


        elif isinstance(error, checks.dev_only):
            embed = discord.Embed(title="❌ That's an issue!", description="This command is for developers only.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)

        elif isinstance(error, checks.max_only):
            embed = discord.Embed(title="❌ That's an issue!", description="This command is for the one and only Max Cross.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)


        elif isinstance(error, commands.CheckFailure):
            embed = discord.Embed(title="❌ That's an issue!", description="Sorry bud but you don't have the correct permissions to run this command.", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)


        elif isinstance(error, commands.CommandOnCooldown):
            embed = discord.Embed(title="❌ That's an issue!", description="This command is on cooldown, retry after {} seconds".format(math.ceil(error.retry_after)), colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)

        elif isinstance(error, commands.MissingPermissions):
            embed = discord.Embed(title="❌ That's an issue!", description="Sorry bud but you can't use that", colour=0xc00505)
            embed.set_footer(text=ver)
            return await ctx.send(embed=embed)



        tra = traceback.format_exception(type(error), error, error.__traceback__, chain=False)
        embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry, i ran into an error i don't know! Here's the traceback: ```py\n%s\n```" % ''.join(tra), file=sys.stderr, colour=0xc00505)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)



    @commands.command()
    async def whoiscreator(self, ctx):
        await ctx.send("Hi i'm cheezy the creator of this bot, if max said he made this bot then do remember that this is built using the Cheese Open Source Project made be me not max")





def setup(bot):
    bot.add_cog(CommandErrorHandler(bot))
