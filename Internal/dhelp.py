import discord
from discord.ext import commands
import random
import json
from Internal import checks
from config.confug import Set

confug = Set()

name = confug.name
ver = confug.version
pre = confug.prefix


class OwnerCog:

    def __init__(self, bot):
        self.bot = bot,

    @commands.command()
    @checks.dev()
    async def dhelp(self, ctx):
        embed=discord.Embed(title="📬 Check your DM'S", color=0x2700c4)
        await ctx.send(embed=embed)
        embed=discord.Embed(title="Support", description="Join the support server incase you have a problem or you need help setting the bot up, to join [Click here](server invite url goes here)", color=0x2dbc01)
        await ctx.author.send(embed=embed)
        embed = discord.Embed(title="Help for {}".format(name), description="The prefix is {}".format(pre), colour=0x2700c4)
        embed.add_field(name=("reload"), value=("Reloads a cog"), inline=False)
        embed.add_field(name=("refresh"), value=("Reloads all the cogs"), inline=False)
        embed.add_field(name=("load"), value=("Loads a cog"), inline=False)
        embed.add_field(name=("unload"), value=("Unloads a cog"), inline=False)
        embed.add_field(name=("botplatform"), value=("Checks what os the bot is being hosted on"), inline=False)
        embed.add_field(name=("say"), value=("Makes the bot say whatever arg it is given"), inline=False)
        embed.add_field(name=("eval"), value=("Evaluates code"), inline=False)
        embed.add_field(name=("discver"), value=("Check what version of the d.py branch the hoster is using"), inline=False)
        embed.add_field(name=("leave"), value=("force the bot to leave the server where the command was executed in"), inline=False)
        embed.add_field(name=("dm"), value=("Makes the bot say whatever arg it is given and then dm it to a user"), inline=False)
        embed.add_field(name=("listguild"), value=("lists all the servers the bot is in"), inline=False)
        embed.set_footer(text=ver)
        await ctx.author.send(embed=embed)



def setup(bot):
    bot.add_cog(OwnerCog(bot))
