import discord
from discord.ext import commands
import platform
import json
import os
import glob
import datetime
import traceback
import logging
import sys
from Internal import checks
from config.confug import Set

confug = Set()

name = confug.name
ver = confug.version
cog = confug.initial_extensions


logging.basicConfig(level=logging.INFO)


logger = logging.getLogger("MassMessager")


class OwnerCog:

    def __init__(self, bot):
        self.b = bot

    def con():
        global con
        con = "1"

    con()

    # Hidden means it won't show up on the default help.
    @commands.command(name='load', hidden=False)
    @checks.dev()
    async def cog_load(self, ctx, *, cog: str):
        try:
            self.b.load_extension(cog)
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')
        else:
            embed = discord.Embed(title = "Success!", description ="{} was successfully loaded".format(cog), color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
            logger.warn(f'{ctx.message.author} has loaded {cog}')

    @commands.command(name='unload', hidden=False)
    @checks.dev()
    async def cog_unload(self, ctx, *, cog: str):
        try:
            self.b.unload_extension(cog)
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')
        else:
            embed = discord.Embed(title = "Success!", description ="{} was successfully unloaded".format(cog), color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
            logger.warn(f'(ctx.message.author) has unloaded {cog}')

    @commands.command(name='reload', hidden=False)
    @checks.dev()
    async def cog_reload(self, ctx, *, cog: str):
        try:
            self.b.unload_extension(cog)
            self.b.load_extension(cog)
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')
        else:
            embed = discord.Embed(title = "Success!", description ="{} was successfully reloaded".format(cog), color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
            logger.warn(f'{ctx.message.author} has reloaded {cog}')


    @commands.command()
    @checks.dev()
    async def conre(self,ctx):
        def con0():
            global con
            con = "0"

        def con1():
            global con
            con = "1"

        con = "0"
        embed=discord.Embed(title="Well?", description="Do you want me to print the refresh list?", color=0x2700c4)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)
        yes_no_response = await self.b.wait_for('message',timeout=10)
        if yes_no_response.content.lower() == "no":
            embed=discord.Embed(title="Ok", description="I will say the refresh list", color=0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
            con0()
            return
        elif yes_no_response.content.lower() == "yes":
            embed=discord.Embed(title="Ok",  description="I will not say the refresh list", color=0x2700c4)
            embed.set_footer(text=ver)
            con1()
        await ctx.send(embed=embed)


    @commands.command()
    @checks.dev()
    async def refresh(self, ctx):
        """Re-initialise the cogs folder."""
        await ctx.send("Please wait...")
        initial_extensions = cog

        for extension in initial_extensions:
            try:
                if con == "0":
                    self.b.unload_extension(extension)
                    self.b.load_extension(extension)
                    await ctx.send(f'Loaded {extension}.')
                elif con == "1":
                    self.b.unload_extension(extension)
                    self.b.load_extension(extension)
                    print(f'Loaded {extension}.')
            except Exception as e:
                print(f'Failed to load extension {extension} because fuck you.', file=sys.stderr)
                traceback.print_exc()

    @commands.command(hidden=True)
    async def geninvite(self, ctx, serverid: str):
        '''Generiert einen Invite für eine Guild wenn möglich (BOT OWNER ONLY)'''
        guild = self.b.get_guild(int(serverid))
        invite = await ctx.guild.create_invite(max_uses=1, unique=False)
        msg = f'Invite für **{guild.name}** ({guild.id})\n{invite.url}'
        await ctx.author.send(msg)




    @commands.command()
    @checks.dev()
    async def leave(self,ctx):
        embed = discord.Embed(title = "👋 Bye bye!", description ="I'm sorry but i must leave this server now", color = 0x2700c4)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)
        server = ctx.guild
        await server.leave()


    @commands.command()
    @checks.dev()
    async def botplatform(self , ctx):
        try:
            embed = discord.Embed(title = "This instance is currently running on:", description = str(platform.platform()), color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
        except:
            embed = discord.Embed(title = "Whoops! ❌", description = "I'm sorry but i could not indentify the operating system", color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)




    @commands.command()
    @checks.dev()
    async def discver(self, ctx):
        embed = discord.Embed(title = "Discord.py branch", description ="I am currently using the {} branch".format(discord.__version__), color = 0x2700c4)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)




    @commands.command()
    @checks.dev()
    async def game(self, ctx, *,args):
        args = args or 'no arg provided'
        game = discord.Game(name=''.join(args))
        await self.b.change_presence(activity=game)
        try:
            await ctx.send("My playing status has been updated to: {}".format(args))
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')


    @commands.command()
    @checks.dev()
    async def rgame(self, ctx):
        game = discord.Game(name=confug.game)
        await self.b.change_presence(activity=game)
        try:
            await ctx.send("My playing status has been updated to: {}".format(confug.game))
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')





    @commands.command()
    @checks.dev()
    async def say(self, ctx, *, message):
        await ctx.message.delete()
        await ctx.send(message)


    @commands.command()
    @checks.dev()
    async def dm(self, ctx, user:discord.Member, *, message):
        await user.send(message)
        await ctx.message.delete()


    @commands.command()
    @checks.dev()
    async def listguild(self,ctx):
        embed = discord.Embed(title = "📬 I sent the list to your DM's", color = 0x2700c4)
        await ctx.send(embed=embed)
        x = '\n'.join([str(guild) for guild in self.b.guilds])
        y = '\n'.join([str(guild.id) for guild in self.b.guilds])
        embed = discord.Embed(title = "I'm in:", description = x, color = 0x2700c4)
        embed.set_footer(text=ver)
        await ctx.author.send(y)
        return await ctx.author.send(embed = embed)


def setup(bot):
    bot.add_cog(OwnerCog(bot))
