from discord.ext import commands
import discord.utils
import asyncio
import json
from config.confug import Set
confug = json.load(open("config\language.json"))
config = Set()

a = confug["2"]

#this is where stuff like @checks.is_dev is made, simply put if your id matches the one on a string the command will work right, else nono


class dev_only(commands.CommandError):
    pass

class max_only(commands.CommandError):
    pass


def dev():
    def predicate(ctx):
        if ctx.author.id in config.dev_ids:
            return True
        else:
            raise dev_only
    return commands.check(predicate)


def max():
    def predicate(ctx):
        if ctx.author.id == '485533138387206144':
            return True
        else:
            raise dev_only
    return commands.check(predicate)
