import discord
from discord.ext import commands
import platform
import json
import datetime
import logging
import inspect
from Internal import checks
from config.confug import Set

config = Set()

logging.basicConfig(level=logging.INFO)
# Configures logging.

logger = logging.getLogger("MassMessager")
# Defines the logger.


name = config.name
ver = config.version

class Eval:

    def __init__(self, bot):
        self.b = bot


#change enabled=True to False if you want the command completely disabled so not even you can use it
    @checks.dev()
    @commands.command(enabled=True)
    async def eval(self, ctx, *, code):
        code = code.strip("` ")
        try:
            result = eval(code)
            if inspect.isawaitable(result):
                result = await result
        except Exception as e:
            embed = discord.Embed(title = "❌ Oops!", description ="Input:` {}`\n{}: ```{}```".format(code, type(e).__name__, e), color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
        else:
            embed = discord.Embed(title = "🛠 Eval", description ="Output: ```{}```".format(result), color = 0x2700c4)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)



def setup(bot):
    bot.add_cog(Eval(bot))
