import discord
from discord.ext import commands
import time
import json
import asyncio
from config.confug import Set

confug = Set()


name = confug.name
ver = confug.version
class OtherCog:

    def __init__(self, bot):
        self.b = bot


    @commands.command()
    @commands.guild_only()
    async def about(self, ctx):
        embed = discord.Embed(title="{}".format(name), description="Hi there! i'm {}, i'm an open source bot using the cosp code created by Cheezy.".format(name), colour=0x2700c4)
        embed.set_footer(text="If you find a bug please report it to Cheezy/Popcorn.")
        embed.set_thumbnail(url="https://cdn.discordapp.com/avatars/472084133825478657/f556357921123f46d655f64f278d5f93.png")
        embed.add_field(name="Am i under 18?", value="Yes, yes i am", inline=True)
        embed.add_field(name="What version am i currently running", value=ver, inline=True)
        await ctx.send(embed=embed)






    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def bomb(self, ctx, messages: int = 100):
        """
        Deletes x amount of messages in a channel.
        """
        await ctx.channel.purge(limit=messages + 1)
        removed = messages + 0
        embed = discord.Embed(title="💣boom!", description="Removed {} messages".format(removed), colour=0x2700c4)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    async def profile(self, ctx, user: discord.Member):
        embed=discord.Embed(title="Heres {}'s info just for you c:".format(user.name), colour=0x2700c4)
        embed.add_field(name="Discriminator:", value=str(user.discriminator), inline=True)
        embed.add_field(name="ID:", value=str(user.id), inline=True)
        embed.add_field(name="Bot Account:", value=str(user.bot), inline=True)
        embed.add_field(name="Highest Role:", value=user.top_role.name, inline=True)
        embed.add_field(name="User:", value=user.mention, inline=True)
        embed.add_field(name="Created:", value=str(user.created_at).split(' ')[0], inline=True)
        embed.add_field(name="Joined:", value=str(user.joined_at).split(' ')[0], inline=True)
        await ctx.send(embed=embed)



    @commands.command()
    @commands.guild_only()
    async def serverinfo(self, ctx):
        embed = discord.Embed(name="{}'s info".format(ctx.message.guild.name), description="This is what i found, somehow.", color=0x2700c4)
        embed.set_footer(text=ver)
        embed.set_author(name="This servers lovely info")
        embed.add_field(name="Name", value=ctx.message.guild.name, inline=True)
        embed.add_field(name="ID", value=ctx.message.guild.id, inline=True)
        embed.add_field(name="Roles", value=len(ctx.message.guild.roles), inline=True)
        embed.add_field(name="Members", value=len(ctx.message.guild.members))
        embed.set_thumbnail(url=ctx.message.guild.icon_url)
        await ctx.send(embed=embed)





    @commands.command(enabled=True)
    @commands.guild_only()
    async def invite(self, ctx):
        embed = discord.Embed(title="Invite me to your server", description="[Click here](https://discordapp.com/oauth2/authorize?client_id=490970593659846676&permissions=66186303&scope=bot)", colour=0x2700c4)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.has_any_role("Staff")
    async def qagree(self, ctx, user: discord.Member):
        try:
            role = discord.utils.get(ctx.guild.roles, name="Member")
            await user.add_roles(role)
            await ctx.send(embed=embed)
        except Exception:
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but there seems to be no role called 'Trusted' or i could not add that role due to a problem with it (it could be higher than my highest role) Please ReRun -setup again", colour=0xc00505)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)


    @commands.command()
    @commands.guild_only()
    async def agree(self, ctx):
        try:
            role = discord.utils.get(ctx.guild.roles, name="Member")
            user = ctx.message.author
            await user.add_roles(role)
            await ctx.message.delete()
        except Exception:
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but there seems to be no role called 'Member' or there is an issue with the role", colour=0xc00505)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    async def ping(self, ctx):
        before = time.perf_counter()
        msg = await ctx.send(embed=discord.Embed(title="Please wait",color=0x2700c4))
        total = (time.perf_counter()-before)*1000
        await msg.edit(embed=discord.Embed(title="🏓 Pong!", description="That took me {}ms and {}.".format(round(total, 1), self.b.latency), success=True, color=0x2700c4))





    @commands.command()
    @commands.has_any_role("Staff")
    async def mute(self, ctx, user: discord.Member):
        try:
            embed = discord.Embed(title="Success!", description="{} has been muted! ✅".format(user.name), colour=0x2dbc01)
            embed.set_footer(text=ver)
            roles_without_everyone = [r for r in user.roles if r.name != "@everyone"]
            await user.remove_roles(*roles_without_everyone)
            role = discord.utils.get(ctx.guild.roles, name="muted")
            await user.add_roles(role)
            await ctx.send(embed=embed)
            for channel in user.guild.channels:
                if channel.name == 'logging':
                    embed = discord.Embed(title = "Warning! ⚠", description = "{} has been muted by {}".format(user.name, ctx.message.author), color = 0xffa500)
                    await channel.send(embed=embed)
        except Exception:
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but there seems to be no role called 'Trusted' or i could not add that role due to a problem with it (it could be higher than my highest role) Please ReRun -setup again", colour=0xc00505)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)



    @commands.command()
    @commands.has_any_role("Staff")
    async def unmute(self, ctx, user: discord.Member):
        try:
            embed = discord.Embed(title="Success!", description="{} has been unmuted! ✅".format(user.name), colour=0x2dbc01)
            embed.set_footer(text=ver)
            role = discord.utils.get(ctx.guild.roles, name="muted")
            await user.remove_roles(role)
            await user.add_roles(discord.utils.get(ctx.guild.roles, name="Member"))
            await ctx.send(embed=embed)
            for channel in user.guild.channels:
                if channel.name == 'logging':
                    embed = discord.Embed(title = "Warning! ⚠", description = "{} has been unmuted by {}".format(user.name, ctx.message.author), color = 0xffa500)
                    await channel.send(embed=embed)
        except Exception:
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but there seems to be no role called 'Trusted' or i could not add that role due to a problem with it (it could be higher than my highest role) Please ReRun -setup again", colour=0xc00505)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)



    @commands.command()
    @commands.has_any_role("Staff")
    async def trust(self, ctx, user: discord.Member):
        try:
            embed = discord.Embed(title="Success!", description="{} has been trusted! ✅".format(user.name), colour=0x2dbc01)
            embed.set_footer(text=ver)
            role = discord.utils.get(ctx.guild.roles, name="Trusted")
            await user.add_roles(role)
            await ctx.send(embed=embed)
        except Exception:
            embed = discord.Embed(title="❌ That's an issue!", description="I'm sorry but there seems to be no role called 'Trusted' or i could not add that role due to a problem with it (it could be higher than my highest role) Please ReRun -setup again", colour=0xc00505)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)






    @commands.command()
    @commands.guild_only()
    async def embed(self, ctx, *, message):
        embed = discord.Embed(title=(ctx.author.name)+" said:", description=(message), colour=0x2700c4)
        await ctx.message.delete()
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(OtherCog(bot))
