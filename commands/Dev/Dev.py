import discord
from discord.ext import commands
import platform
import json
import datetime
from Internal import checks
from config.confug import Set 

confug = Set()

name = confug.name
ver = confug.version

class OwnerCog:

    def __init__(self, bot):
        self.b = bot
    



    # Hidden means it won't show up on the default help.
    @commands.command(name='load', hidden=False)
    @checks.dev()
    @checks.dmo()
    async def cog_dload(self, ctx, *, cog: str):
        try:
            self.b.load_extension(cog)
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')
        else:
            embed = discord.Embed(title = "Success!", description ="{} was successfully loaded", color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)

    
    @commands.command(name='unload', hidden=False)
    @checks.dev()
    @checks.dmo()
    async def cog_dunload(self, ctx, *, cog: str):
        try:
            self.b.unload_extension(cog)
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')
        else:
            embed = discord.Embed(title = "Success!", description ="{} was successfully unloaded", color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)

    @commands.command(name='reload', hidden=False)
    @checks.dev()
    @checks.dmo()
    async def cog_dreload(self, ctx, *, cog: str):
        try:
            self.b.unload_extension(cog)
            self.b.load_extension(cog)
        except Exception as e:
            await ctx.send(f'**`ERROR:`** {type(e).__name__} - {e}')
        else:
            embed = discord.Embed(title = "Success!", description ="{} was successfully reloaded", color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)


    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def dleave(self,ctx):
        embed = discord.Embed(title = "👋 Bye bye!", description ="I'm sorry but i must leave this server now", color = 0x760dd8)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)
        server = ctx.guild
        await server.leave()


    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def dbotplatform(self , ctx):
        try:
            embed = discord.Embed(title = "This instance is currently running on:", description = str(platform.platform()), color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
        except:
            embed = discord.Embed(title = "Whoops! ❌", description = "I'm sorry but i could not indentify the operating system", color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
    '''
    @commands.command()
    @checks.dmo()
    async def status(self, ctx, *, message: str):
        new_status = discord.Game(name=message.format)
        await self.b.change_presence(activity=new_status)
        self.b.status_format = message
        await ctx.send("who fucxking knows")

			
			
    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def discver(self, ctx):
        embed = discord.Embed(title = "Discord.py branch", description ="I am currently using the {} branch".format(discord.__version__), color = 0x760dd8)
        embed.set_footer(text=ver)
        await ctx.send(embed=embed)



    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def dol(self, ctx, *, message):
        channel = self.b.get_channel(435904797997137920)
        await ctx.message.delete()
        await channel.send(message)


    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def da(self, ctx):
        guild = self.b.get_guild(377847211196678147)
        await ctx.send("Server name: {}".format(guild.name))
        await ctx.send("Owner: {}".format(guild.owner))
        await ctx.send("Created at: {}".format(guild.created_at))
        await ctx.send("Member count: {}".format(guild.member_count))
        await ctx.send("Splash: {}".format(guild.splash_url))
        await ctx.send("Shard id: {}".format(guild.shard_id))
        await ctx.send("icon url: {}".format(guild.icon_url))
        await ctx.send("Verification level: {}".format(guild.verification_level))
        await ctx.send("Id: {}".format(guild.id))







    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def dll(self, ctx, *, message):
        channel = self.b.get_channel(435904767152357377)
        await ctx.message.delete()
        await channel.send(message)


    @commands.command()
    @checks.dev()
    async def say(self, ctx, *, message):
        await ctx.message.delete()
        await ctx.send(message)


    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def dm(self, ctx, user:discord.Member, *, message):
        await user.send(message)
        await ctx.message.delete()


    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def listguild(self,ctx):
        embed = discord.Embed(title = "📬 I sent the list to your DM's", color = 0x760dd8)
        await ctx.send(embed=embed)
        x = '\n'.join([str(guild) for guild in self.b.guilds])
        y = '\n'.join([str(guild.id) for guild in self.b.guilds])
        embed = discord.Embed(title = "I'm in:", description = x, color = 0x760dd8)
        embed.set_footer(text=ver)
        await ctx.author.send(y)
        return await ctx.author.send(embed = embed)







    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def em(ctx):
        col = 0xc00505
        embed=discord.Embed(title=" embed title string1",  description="embed description string2", color=col)
        embed.set_footer(text="embed footer string3")
        await ctx.send(embed=embed)




    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def ki(self, ctx, user:discord.Member,*, message):
        await ctx.message.delete()
        await user.send(message)
        await ctx.guild.kick(user)

    @commands.command()
    @checks.dev()
    @checks.dmo()
    async def bi(self, ctx, user:discord.Member,*, message):
        await ctx.message.delete()
        await user.send(message)
        await ctx.guild.ban(user)
    '''





























def setup(bot):
    bot.add_cog(OwnerCog(bot))
