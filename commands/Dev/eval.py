import discord
from discord.ext import commands
import platform
import json
import datetime
import inspect
from Internal import checks
from config.confug import Set 

confug = Set()

name = confug.name
ver = confug.version

class Eval:

    def __init__(self, bot):
        self.b = bot


    @checks.dev()
    @checks.dmo()
    @commands.command()
    async def deval(self, ctx, *, code):
        code = code.strip("` ")
        try:
            result = eval(code)
            if inspect.isawaitable(result):
                result = await result
        except Exception as e:
            embed = discord.Embed(title = "❌ Oops!", description ="Input:` {}`\n{}: ```{}```".format(code, type(e).__name__, e), color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)
        else:
            embed = discord.Embed(title = "🛠 Eval", description ="Output: ```{}```".format(result), color = 0x760dd8)
            embed.set_footer(text=ver)
            await ctx.send(embed=embed)



def setup(bot):
    bot.add_cog(Eval(bot))